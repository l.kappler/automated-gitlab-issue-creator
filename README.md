# GitLab Issue Creator

This script automates the process of creating an issue in GitLab with certain parameters like assignee and due date.

## Security Risks

When you create a personal access token as needed here, it is essentially an alternative for your username and password. If someone gets hold of your token, they have full access to your account and data.

Therefore, it is critically important to:

- Keep your personal access tokens secret.
- Never expose them in any public places such as GitHub repositories.
- Revoke your personal access token immediately if you suspect it has been leaked or when it's not in use anymore.

Also, as a best practice, always limit the scope of personal access tokens. Our script requires the 'api' scope to write to non-private repos. (I might be wrong though)

## Setup 

1. First, install required Python packages using pip:
```sh
pip install requests gitpython configparser
```

2. Generate your *Personal Access Token* in GitLab.
    - Sign in to GitLab.
    - In the top-right corner, click on your avatar and select "Settings".
    - In the left sidebar, click on "Access Tokens".
    - Choose a name and optional expiry date for the token.
    - Choose the 'api' scope.
    - Click the "Create personal access token" button.
    - Save the personal access token somewhere safe. Once you leave or refresh the page, you won't be able to access it again.

3. Save your personal access token inside a text file and note the path to this file.

4. Create a `config.ini` file in the same directory as the python script, and fill in your token file path like so:
    ```ini
    [DEFAULT]
    TOKEN_PATH = path_to_your_token_file (use an absolute path)
    ISSUE_DESCRIPTION_FILE = corrections/current.md (use a relative path from within the git repo.)
    ```



   
## Usage

This script will create a new issue under the GitLab repository of the origin remote of the git-repo in the directory it is run from. It will set the owner of the repo as the assignee and accept the due date for the issue from a command line argument. The title and description of the issue will be taken from the file given in `config.ini` The first line will be taken as title.

1. Clone the repository using ssh(!). (The username of the owner must be part of that url with the format: f"git@{server}:{username}/{project_name}.git")

2. create the file containing the issue title in the first line and the description in the rest at the path given in ISSUE_DESCRIPTION_FILE relative from the git repo.

3. The script accepts the due date as a command line parameter. Run the script from within the git-repo like:
```sh
python path/to/create_issue.py 2022-12-31
```

Tipp: you can use a `.gitignore` file containing `*` in the `corrections/` folder to ignore all your issue descriptions in that folder. Note that files will be overwritten, when pulling a file with the same name.

After the issue is created on GitLab, the URL to the issue will be printed in the console and also appended to the issue's markdown file.

### Applying to Subdirectories with Fish

Fish is a user-friendly command line shell for Unix-like operating systems. We can use Fish to recursively apply our issue creation script (or any other command) to every Git repository in subdirectories of the current directory.

First, you need to save the Fish function. This can be done by creating a function in a new fish script file in the `~/.config/fish/functions/` directory. If this directory does not exist, you can create it.

For example, you can create the function in a file named `applyToSubfolders.fish`:

```
# ~/.config/fish/functions/applyToSubfolders.fish
function applyToSubfolders
    for dir in ./*
        if test -d $dir
            echo
            echo
            echo -e "\033[35mIn directory $dir\033[0m"
            cd $dir
            eval $argv
            cd ..
        end
    end
end
```

This function will now be automatically loaded by Fish and you can use it in any new shell session.

Now, to use this function alongside your script:

- Navigate to the directory containing your repos.
- Run the following command:
    ```applyToSubfolders 'python path_to_your_script/script.py YYYY-MM-DD'```

This command will run the script in each subdirectory, creating an issue on each corresponding GitLab repository. Replace `path_to_your_script` with the actual path to your Python script, and replace `YYYY-MM-DD` with your desired due date.

Tipp: If you have non git-repo subdirs you can add a check for a `$dir/.git/` directory before entering `$dir`. I like to use this for non-git repos as well in other contexts.

This setup works only when all subdirectories are GitLab repositories and have the necessary issue markdown files. Otherwise the function will stop in the folder that caused the error.

## Credits

Most of this project was written by GPT-4. (Who would have thought... this ReadMe was way to wordy for a normal person to write it.)
