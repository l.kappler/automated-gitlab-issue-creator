test automating issue

- [ ] test of bullet list
- [ ] example of how to keep stuf indented:
    Single new line characters are ignored. So you need a single `\` for a new line: \
    This is indented and on a new line.
    ```
    This is also indented. But just because the ticks started as indented already.
    ```
    After a Code block a new line must always start, so we don't need the `\`. However for the next line we do again:\
    This is indented.


This isn't anymore.

- [x] checked bullet.

```python
def testing_python_code():
    pass
```

<details>
<summary>This issue was automatically created</summary>

The code used to create this issue can be found at https://gitlab.gwdg.de/l.kappler/automated-gitlab-issue-creator/
</details>
