import requests
import re
import json
import argparse
import configparser
import os
from pathlib import Path
from git import Repo


# Argument Parser
parser = argparse.ArgumentParser(description='Create Gitlab issue')
parser.add_argument('due_date', type=str, help='Due date for the issue in YYYY-MM-DD format')
args = parser.parse_args()

# Read config.ini for token file path
config = configparser.ConfigParser()
config.read(f'{Path(__file__).parent.absolute()}/config.ini')
token_file_path = config['DEFAULT']['TOKEN_PATH']
ISSUE_DESCRIPTION_FILE = config['DEFAULT']['ISSUE_DESCRIPTION_FILE']

# Read the token from the specified file
with open(os.path.expanduser(token_file_path), 'r') as file:
    private_token = file.read().strip()

# Headers for GitLab.
headers = {'PRIVATE-TOKEN': private_token}


def get_username_gitlab_url_and_project_id(repo):
    url = repo.remotes.origin.url
    matching = re.search('git@(.*):(.*)/(.*)\.git$', url)
    username = matching.group(2)
    server = matching.group(1)
    project_name = matching.group(3)
    gitlab_url = f"https://{server}"

    # get project_id
    response = requests.get(f'{gitlab_url}/api/v4/projects/{username}%2F{project_name}',
                            headers=headers)
    if response.status_code == 200:
        project_id = response.json()['id']
    else:
        print("project_id not found for", url)
        project_id = None
    return username, gitlab_url, project_id

# Get username and gitlab_url from repo url
repo = Repo('.')
username, gitlab_url, project_id = get_username_gitlab_url_and_project_id(repo)


# Get the user_id from GitLab users API.
response = requests.get(f'{gitlab_url}/api/v4/users?username={username}', headers=headers)
user_id = response.json()[0]['id']

# Load title and description from issue_description_file
with open(ISSUE_DESCRIPTION_FILE, 'r') as file:
    data = file.read().split('\n', 1)
    title = data[0]
    if len(data) > 1:
        description = data[1]
    else:
        description = ""

# Payload to create issue.
payload = {
    'title': title,
    'description': description,
    'assignee_ids': [user_id],
    'due_date': args.due_date,
}

response = requests.post(f'{gitlab_url}/api/v4/projects/{project_id}/issues', headers=headers, data=payload)

# Print the result.
if response.status_code == 201:
    print('Issue created successfully.')
    issue_url = response.json()['web_url']
    print("URL of the Issue: ", issue_url)
    # Append the URL to the description file.
    with open(ISSUE_DESCRIPTION_FILE, 'a') as file:
        file.write(f"\n\nIssue URL: {issue_url}")
else:
    print(json.loads(response.content))


